from typing import Any, Protocol

from textx import metamodel_from_file

from karel_robot.run import *


class Handler(Protocol):
    @classmethod
    def handle(self, command) -> None:
        pass


class MoveHandler:
    @classmethod
    def handle(self, command) -> None:
        move()


class TurnHandler:
    @classmethod
    def handle(self, command) -> None:
        if command.direction == "left":
            turn_left()

        if command.direction == "right":
            turn_right()


class ExitHandler:
    @classmethod
    def handle(self, command) -> None:
        exit()


class BeeperHandler:
    @classmethod
    def handle(self, command) -> None:
        if command.action == "pick":
            pick_beeper()

        if command.action == "put":
            put_beeper()


def get_name(statement) -> str:
    return statement if isinstance(statement, str) else statement.__class__.__name__


DEFAULT_CONTEXT = {"break": False}


class Procedure:
    def __init__(self, args: list, commands: list) -> None:
        self.args = args
        self.commands = commands


class Interpreter:
    def __init__(self) -> None:
        self._handlers: dict[str, Handler] = {}
        self._procedures: dict[str, Procedure] = {}
        self._stack = [DEFAULT_CONTEXT]

    def register_handler(self, name: str, handler: Handler) -> None:
        self._handlers[name] = handler

    def interpret(self, model) -> None:
        self.process_commands(model.commands)

    def process_commands(self, commands: list) -> None:
        for command in commands:
            self.process_command(command)

    def process_command(self, command) -> None:
        if self._stack[-1]["break"]:
            return

        name = get_name(command)

        if (handler := self._get_handler(name)) is not None:
            handler.handle(command)
            return

        if name == "Procedure":
            self._add_procedure(command)
            return

        if name == "Call":
            self._process_call(command)
            return

        if name == "StatementIf":
            self._process_if(command)
            return

        if name == "StatementWhile":
            self._process_while(command)
            return

        if name == "StatementFor":
            self._process_for(command)
            return

    def _process_if(self, statement):
        if self.process_expression(statement.condition):
            self.process_commands(statement.commands)
        else:
            self.process_commands(statement.else_commands)

    def _process_while(self, statement):
        while self.process_expression(statement.condition):
            if self._stack[-1]["break"]:
                self._stack[-1]["break"] = False
                break

            self.process_commands(statement.commands)

    def _process_for(self, statement):
        for _ in range(statement.n):
            if self._stack[-1]["break"]:
                self._stack[-1]["break"] = False
                break

            self.process_commands(statement.commands)

    def _get_handler(self, command) -> Handler:
        return self._handlers.get(command)

    def process_expression(self, expression):
        name = get_name(expression)

        match name:
            case "Or":
                return self.process_expression(
                    expression.left
                ) or self.process_expression(expression.rigth)
            case "And":
                return self.process_expression(
                    expression.left
                ) and self.process_expression(expression.rigth)
            case "Not":
                return not self.process_expression(expression.expression)
            case "is_beeper":
                return beeper_is_present()
            case "front_is_treasure":
                return front_is_treasure()
            case "front_is_blocked":
                return front_is_blocked()
            case "north":
                return facing_north()
            case "east":
                return facing_east()
            case "west":
                return facing_west()
            case "south":
                return facing_south()
            case _:
                raise RuntimeError(f"Unknown expression: {str(expression)}")

    def _add_procedure(self, procedure) -> None:
        self._procedures[procedure.name] = Procedure(procedure.args, procedure.commands)

    def _process_call(self, call) -> None:
        self._stack.append(DEFAULT_CONTEXT)

        if (procedure := self._procedures.get(call.name.name)) is None:
            raise RuntimeError(f"unable to find procedure {call.name.name}")

        for i, arg in enumerate(call.args):
            self._stack[-1][procedure.args[i]] = arg

        self.process_commands(procedure.commands)

        self._stack.pop()


metamodel = metamodel_from_file("metamodel.tx")
program = metamodel.model_from_file("program.karel")

interpreter = Interpreter()
interpreter.register_handler("move", MoveHandler)
interpreter.register_handler("Turn", TurnHandler)
interpreter.register_handler("exit", ExitHandler)
interpreter.register_handler("Beeper", BeeperHandler)

interpreter.interpret(program)
