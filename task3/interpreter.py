from typing import Protocol

from textx import metamodel_from_file

from karel_robot.run import (
    beeper_is_present,
    facing_east,
    facing_north,
    facing_south,
    facing_west,
    front_is_blocked,
    front_is_treasure,
    move,
    pick_beeper,
    put_beeper,
    turn_left,
    turn_right,
)


class Handler(Protocol):
    @classmethod
    def handle(self, command) -> None:
        pass


class MoveHandler:
    @classmethod
    def handle(self, command) -> None:
        move()


class TurnHandler:
    @classmethod
    def handle(self, command) -> None:
        if command.direction == "left":
            turn_left()

        if command.direction == "right":
            turn_right()


class ExitHandler:
    @classmethod
    def handle(self, command) -> None:
        exit()


class BeeperHandler:
    @classmethod
    def handle(self, command) -> None:
        if command.action == "pick":
            pick_beeper()

        if command.action == "put":
            put_beeper()


def get_name(statement) -> str:
    return statement if isinstance(statement, str) else statement.__class__.__name__


class Interpreter:
    def __init__(self) -> None:
        self._handlers: dict[str, Handler] = {}

    def register_handler(self, name: str, handler: Handler) -> None:
        self._handlers[name] = handler

    def interpret(self, model) -> None:
        self.process_commands(model.commands)

    def process_commands(self, commands: list) -> None:
        for command in commands:
            self.process_command(command)

    def process_command(self, command) -> None:
        name = get_name(command)

        if (handler := self._get_handler(name)) is not None:
            handler.handle(command)
            return

        if name == "StatementIf":
            self._process_if(command)
            return

        if name == "StatementWhile":
            self._process_while(command)
            return

        if name == "StatementFor":
            self._process_for(command)
            return

    def _process_if(self, statement):
        if self.process_expression(statement.condition):
            self.process_commands(statement.commands)
        else:
            self.process_commands(statement.else_commands)

    def _process_while(self, statement):
        while self.process_expression(statement.condition):
            self.process_commands(statement.commands)

    def _process_for(self, statement):
        for _ in range(statement.n):
            self.process_commands(statement.commands)

    def _get_handler(self, command) -> Handler:
        return self._handlers.get(command)

    def process_expression(self, expression):
        name = get_name(expression)

        match name:
            case "Or":
                return self.process_expression(
                    expression.left
                ) or self.process_expression(expression.rigth)
            case "And":
                return self.process_expression(
                    expression.left
                ) and self.process_expression(expression.rigth)
            case "Not":
                return not self.process_expression(expression.expression)
            case "is_beeper":
                return beeper_is_present()
            case "front_is_treasure":
                return front_is_treasure()
            case "front_is_blocked":
                return front_is_blocked()
            case "north":
                return facing_north()
            case "east":
                return facing_east()
            case "west":
                return facing_west()
            case "south":
                return facing_south()
            case _:
                raise RuntimeError(f"Unknown expression: {str(expression)}")


metamodel = metamodel_from_file("metamodel.tx")
program = metamodel.model_from_file("program.karel")

interpreter = Interpreter()
interpreter.register_handler("move", MoveHandler)
interpreter.register_handler("Turn", TurnHandler)
interpreter.register_handler("exit", ExitHandler)
interpreter.register_handler("Beeper", BeeperHandler)

interpreter.interpret(program)
