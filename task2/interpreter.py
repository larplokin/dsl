from typing import Protocol

from textx import metamodel_from_file

from karel_robot.run import move, pick_beeper, put_beeper, turn_left, turn_right


class Handler(Protocol):
    @classmethod
    def handle(self, command) -> None:
        pass


class MoveHandler:
    @classmethod
    def handle(self, command) -> None:
        move()


class TurnHandler:
    @classmethod
    def handle(self, command) -> None:
        if command.direction == "left":
            turn_left()

        if command.direction == "right":
            turn_right()


class ExitHandler:
    @classmethod
    def handle(self, command) -> None:
        exit()


class BeeperHandler:
    @classmethod
    def handle(self, command) -> None:
        if command.action == "pick":
            pick_beeper()

        if command.action == "put":
            put_beeper()


class Interpreter:
    def __init__(self) -> None:
        self._handlers: dict[str, Handler] = {}

    def register_handler(self, name: str, handler: Handler) -> None:
        self._handlers[name] = handler

    def interpret(self, model) -> None:
        for command in model.commands:
            self.process_command(command)

    def process_command(self, command) -> None:
        handler = self._get_handler(
            command if isinstance(command, str) else command.__class__.__name__
        )
        handler.handle(command)

    def _get_handler(self, command) -> Handler:
        return self._handlers.get(command)


metamodel = metamodel_from_file("metamodel.tx")
program = metamodel.model_from_file("program.karel")

interpreter = Interpreter()
interpreter.register_handler("move", MoveHandler)
interpreter.register_handler("Turn", TurnHandler)
interpreter.register_handler("exit", ExitHandler)
interpreter.register_handler("Beeper", BeeperHandler)

interpreter.interpret(program)
